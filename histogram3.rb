require 'pp'

bag = Hash.new(0)

cutoff = 2
if ARGV[0] != nil
   cutoff = ARGV[0].to_i
end

puts cutoff

$stdin.each do |line|
  line.chomp!
  line.downcase!
  line.gsub!(/[^a-zA-Z\s]/, '')
  line.sub!(/^\s+/, '')
  words = line.split(/ +/)
  words.each { |word| bag.store(word, bag[word] += 1) }
end

#Create a sorted array of the values from most occurring to least
#Sort the words that have the same number of occurrences in alphanumeric order
bagarray = bag.sort_by {|k, v| [-v,k]}


#Trim out the words that occur less than the cut off
bagarray.select! {|k,v| v >= cutoff }

#Get the longest words length
longest = bagarray.inject(0) {|max, wordpair|
  max < wordpair[0].length ? wordpair[0].length : max
}

#Print the list all pretty
bagarray.each do | apair |
  printf '%-*.*s ', longest, longest, apair[0]
  puts "*" * apair[1]
end