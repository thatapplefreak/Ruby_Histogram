bag = Hash.new(0)

$stdin.each do |line|
  line.chomp!
  line.downcase!
  line.gsub!(/[^a-zA-Z\s]/, '')
  line.sub!(/^\s+/, '')
  words = line.split(/ +/)
  words.each { |word| bag.store(word, bag[word] += 1) }
end

selectedBag = bag.select {|k,v| v >= 2 }
selectedBag.each { |k,v| puts "#{k} occurs #{v} times"}